<?php
// 指定允许其他域名访问
header('Access-Control-Allow-Origin:*'); //或指定域
// 响应类型
header('Access-Control-Allow-Methods:GET,POST,OPTIONS, PUT, DELETE');
// 响应头设置
header('Access-Control-Allow-Headers:x-requested-with,content-type');
//连接数据库
//（服务器的主机名,用户名,密码.数据库）
$con = new mysqli("localhost", "QQ", "123456", "qq");
//判断数据库连接是否成功
if ($con->connect_error) {
    die("连接失败:" . $con->connect_error);
}
//编码格式
$con->query("set names 'utf8'");
//判断传输方式
if ($_GET) {
    $type = $_GET["type"];
} else {
    $type = $_POST["type"];
};
//根据不同的type值进行不同的操作
switch ($type) {
    case "xiazai":
        $downlad = "downlad";
        Sc($downlad);
        break;
    case "dongtai":
        $downlad = "dynamic";
        Sc($downlad);
        break;
    case "huiyuan":
        $downlad = "member-img";
        Sc($downlad);
        break;
    case "huiyuanlunbo":
        $downlad = "member-slideshow";
        Sc($downlad);
        break;
    case "register":
        $downlad = "register";
        Sc($downlad);
        break;
};
//关闭数据库连接
$con->close();
//   数据库数据全局查找
function Sc(&$name)
{
    //获取全局变量
    global $con;
    $sql = "SELECT * FROM `$name` WHERE 1";
    $result = $con->query($sql);
    $arr = array();
    // 输出每行数据 
    while ($row = $result->fetch_assoc()) {
        $count = count($row); //不能在循环语句中，由于每次删除row数组长度都减小 
        for ($i = 0; $i < $count; $i++) {
            unset($row[$i]); //删除冗余数据 
        };
        array_push($arr, $row);
    };
    echo json_encode($arr, JSON_UNESCAPED_UNICODE);
}
