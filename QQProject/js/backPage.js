//   创建元素
$('body').append($("<button class = 'backBtn'>后台管理</button>"));
$(".backBtn").click(function () {
    dataBtnMove();
});
//   创建页面元素
function dataBtnMove() {
    $("body").append($("<div class = 'layer'></div>"));
    $(".layer").css("width", window.innerWidth).css("height", window.innerHeight);
    $(".layer").append($("<div class = 'backModal'><input class = 'reInp' type = 'text' placeholder = '请输入账号'><input class = 'loInp' type = 'password' placeholder = '请输入密码'><button class = 'loBtn'>登录</button></div>"));
    $(".backModal").css("left", ($(".layer").css("width").split("px")[0] - $(".backModal").css("width").split("px")[0]) / 2 + "px").append($("<div class = 'closeBtn'></div>"));
    //   关闭按钮hover效果
    $(".closeBtn").hover(
        function () {
            $(this).css("transform", "scale(1.1)");
        },
        function () {
            $(this).css("transform", "");
        }
    )
    //   关闭按钮点击事件
    $(".closeBtn").click(function () {
        $(".layer").remove();
    })
    //   登录按钮按下事件
    $(".loBtn").mousedown(function () {
        $(this).css("transform", "scale(0.8)");
    }).mouseup(function () {
        //   登录按钮抬起事件
        $(".loBtn").css("transform", "");
        if ($(".reInp").val() == 123456789 && $(".loInp").val() == 123456789) {
            $(".reInp").text("");
            $(".loInp").text("");
            window.open("http://localhost/QQproject/backstage.html");
        } else {
            window.open("https://www.baidu.com/s?wd=如何破解后台管理系统");
        }
    })
    //   body的鼠标抬起事件
    $("body").mouseup(function () {
        $(".loBtn").css("transform", "");
    })
}