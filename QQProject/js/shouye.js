// 首页图片切换
$(".on1").mouseover(function () {
    $(".mqq").fadeTo("slow", 1);
    $(".pcqq").fadeTo("slow", 0);
    $(".on1").css("background", "url(https://sqimg.qq.com/qq_product_operations/im/2018/navcur.png)no-repeat center center");
    $(".on").css("background", "url(https://sqimg.qq.com/qq_product_operations/im/2018/nav.png)no-repeat center center")
})
$(".on").mouseover(function () {
    $(".pcqq").fadeTo("slow", 1);
    $(".mqq").fadeTo("slow", 0);
    $(".on").css("background", "url(https://sqimg.qq.com/qq_product_operations/im/2018/navcur.png)no-repeat center center");
    $(".on1").css("background", "url(https://sqimg.qq.com/qq_product_operations/im/2018/nav.png)no-repeat center center")
})

// 滚轮事件
//头部的背景和下边框
$(function () {
    $(window).scroll(function () {
        if ($(document).scrollTop() > 1000) {
            $(".topbar").css("background", "#fff");
            $(".topbar").css("border-bottom", "1px solid #f1f1f1");
            $(".topicfixed").css("height", "75px");
            $(".topicfixed").css("position", "fixed");
        }
        if ($(document).scrollTop() < 1000) {
            $(".topbar").css("background", "transparent");
            $(".topbar").css("border", "none");
            $(".topicfixed").css("position", "absolute")
        }
    })
})

var yy;
// 背景图滚轮事件
var scrollFunc = function (e) {
    var direct = 0;
    e = e || window.event;
    var scroll = $(this).scrollTop();//当前滚动高度
    yy = $(".activebg").css("background-position-y");
    yy = yy.split("px")[0];
    if (e.wheelDelta) { //判断浏览器TE，谷歌
        if (scroll == 0) {
            return
        }
        if (e.wheelDelta > 0) {
            yy -= 1;
            $(".activebg").css("background-position-y", yy + "px");
        }
        if (e.wheelDelta < 0) {
            yy += 1;
            $(".activebg").css("background-position-y", yy + "px");
        }
    } else if (e.detail) { //Firefox滑动
        if (scroll == 0) {
            return
        } else if (e.detail > 0) {//向上滚动时
            yy -= 1;
            $(".activebg").css("background-position-y", yy + "px");
        }
        if (e.detail < 0) {//向下移动
            yy += 1;
            $(".activebg").css("background-position-y", yy + "px");
        }
    }
}
//给页面绑定滚轮事件
// if (document.addEventListener) {
//     document.addEventListener("DOMMouseScroll", scrollFunc, false);
// }
// window.onmousewheel = document.onmousewheel = scrollFunc;

$(".qblog").mouseover(function () {
    $(".pic1").css("opacity", 1);
    $(".pic1").css("animation", "zoomIn 0.2s cubic-bezier(0, .25, .08, 1) 0.5s");
    $(".pic1").css("animation-fill-mode", "backwards")
    $(".pic1").css("transition", "0.4");
    $(".pic1").css("width", "150px")
    $(".pic1").css("height", "150px")
    $(".pic2").css("opacity", 1);
    $(".pic2").css("zoomIn 0.2s cubic-bezier(0, .25, .08, 1) 0.8s;");
    $(".pic2").css("animation-fill-mode", "backwards")
    $(".pic2").css("transition", "0.6s");
    $(".pic2").css("width", "150px")
    $(".pic2").css("height", "150px")
    $(".pic3").css("opacity", 1);
    $(".pic3").css("zoomIn 0.2s cubic-bezier(0, .25, .08, 1) 1s");
    $(".pic3").css("animation-fill-mode", "backwards")
    $(".pic3").css("transition", "0.8s");
    $(".pic3").css("width", "150px")
    $(".pic3").css("height", "150px")
})
$(".qblog").mouseout(function () {
    $(".pic1").css({
        "opacity": 0,

    })
    $(".pic2").css({
        "opacity": 0,
    })
    $(".pic3").css({
        "opacity": 0,
    })
})




