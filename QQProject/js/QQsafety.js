$(".wrap").append($('<div class="banner"></div><div class="content"></div><div class="more"></div><div class="Bottom"></div>'));//三部分
$(".wrap .banner").append($('<div class="banner_header"></div><div class="banner_base"><p>公告：至尊保功能调整</p></div>'));//顶部栏
$(".banner_header").append($('<div class="header_wrap"></div>'))//居于中间
$(".header_wrap").append($('<div class="header_left"><img src="../img/ico_logo.png"></div><div class="header_right"></div>'));//分为两部分。左边logo，右边选项
$(".header_right").append($(' <div>反馈问题</div><div>手机App</div><div></div><div onclick = "fn()">登陆</div>'));
/* 第二块密码管理等大块 */
// $(".content").append($('<div class="content_wrap"><div class="content_one"></div><div class="content_two"></div><div class="content_there"></div></div>'));
$(".content").append($('<div class="content_wrap"></div>'));
//坐标数组
var coorArr = ["337px -7px;", "337px -268px;", "337px -528px"];//大图坐标数组
var iconArr = ["0 -234px", "0 -115px", "0 5px"];//小图标数组
var textArr = [["密码管理", "找回、修改QQ密码，从这里开始"], ["密保工具", "设置安全保护，快速解决帐号异常"], ["账号解封", "解除帐号限制，恢复登录"]];//文字数组
for (var i = 0; i < coorArr.length; i++) {
    $(".content_wrap").append($('<div class="content_elem"><i class="sprite_content"></i><p class="title">' + textArr[i][0] + '</p><p class="desc"></p>' + textArr[i][1] + '</div>'));
}
//更多功能栏
$(".more").append($('<div class="more_wrap"><div class="more_title">更多功能</div><div class="more_line"></div><div class="more_bottom"></div></div>'));

var image_textArr = [["-16px -290px", "独立密码", "忘记独立密码可重新设置"], ["-16px -167px", "QQ登录保护", "登录验证密保，阻止坏人登录"], ["-16px -110px", "游戏保护", "登录验证密保，保护游戏财产"], ["-16px -58px", "邮箱保护", "登录验证密保，防止隐私泄漏"]];//图片坐标，文字数组
for (var a = 0; a < image_textArr.length; a++) {
    $(".more_bottom").append($('<div class="more-elem"> <div></div><p>' + image_textArr[a][1] + '</p><p>' + image_textArr[a][2] + '</p></div>'));
}

//最底部相关信息
$(".Bottom").append($('<p>关于腾讯 | About Tencent | 服务条款 | 腾讯招聘 | 隐私政策 | 帮助中心</p><p>Copyright © 1998 - 2020  Tencent. All Rights Reserved. </p>'))
var fn = () => {
    open("#")
}