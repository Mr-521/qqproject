//创建一个logo
var logo = $("<a  class='logo'>QQ</a>");
//添加到页面中
$(".wrap").append($(logo));
//注册页面左边的大图盒子
var side = $("<div class='side'></div>");
//添加到页面
$(".wrap").append($(side));

// 左边大图框里的图的数组
var imgArr = ["../source/image/regist/01-1.jpg", "../source/image/regist/01-3.jpg", "../source/image/regist/01-4.jpg"];
var ImgJs = 1;
setInterval(function () {
    $(side).css("background-image", "url(" + imgArr[ImgJs] + ")");
    ImgJs++;
    if (ImgJs >= imgArr.length) {
        ImgJs = 0;
    }
}, 3000)

//右边的导航栏
var Top = $('<div class="top"></div>');//创建一个导航栏div
$(".wrap").append($(Top));//放进页面
$(Top).append($('<div class="opinion"></div><div class="familiar"></div><div class="surfing"></div>'));//创三个盒子放进导航栏

$(".opinion").append($('<a href="">意见反馈</a>'));//意见反馈
$(".familiar").append($('<div>简体中文</div><ul><li>繁體中文</li><li>English</li></ul>'));//语言选项

$(".surfing").append($('<a href=""></a><div class="hobby"></div>'))//QQ靓号,分两部分，外面露出来的a标签，和隐藏起来的div
$(".surfing a").append($('<div>QQ靓号</div>'));//红底白字
$(".surfing a").append($('<img src="../source/image/regist/logo3.png" alt="">'));//红色小企鹅

$(".hobby").append($('<div class="upper"></div><div class="bottom"></div>'));//隐藏框

////隐藏框头部内容
$(".upper").append($('<a href=""><div></div></a><div class="search"></div>'));//放大镜图标引入icon字体
$(".search").append($('<input type="text" placeholder="搜索QQ靓号，如“1992”，“520”">'));

//隐藏框下部内容
$(".bottom").append($('<div class="sign"><a href="">生日</a><a href="">爱情</a><a href="">星座</a><a href="">手机</a></div>'));

//欢迎注册QQ那一栏
$(".wrap").append($('<div class="mainsuper"></div>'));//添加到页面中

$(".mainsuper").append($('<div class="main"></div>'));//小一号的盒子
$(".main").append($('<div class="from"></div><div class="footer">Copyright © 1998-2020Tencent All Rights Reserved</div>'));//更小的块就是注册的那一大块

$(".from").append($('<div class="wel"></div><div class="import"></div>'))//注册那一大块分为三个小块（大字那一块，输入框一块，底部一块）

$(".wel").append($('<div class="welcome">欢迎注册QQ</div><a href="" class="head">免费靓号</a><div class="header">每一天乐在沟通</div>'));//注册那一小块也分为三个小块（我为什么要用”也“？）

$(".import").append($('<div class="inputA"></div><div class="inputB"></div><div class="cellphone"></div><div class="hint"></div><div class="Immediately"></div><div class="agreement"></div>'));//输入框那一块，一共分为7小块
$(".hint").append("<div>可通过该手机号找回密码</div><div class='judge'></div>")//
//第一个输入框里有两个盒子，一个是input。一个是正则显示框
$(".inputA").append($('<div class="input_a"></div><div class="input_b"></div>'));
//昵称输入框
$(".input_a").append($('<input type="text" placeholder="昵称" id = "Rnick">'))
//正则
// 昵称 中文、英文、数字但不包括下划线等符号
// var nickname = /[\u4e00-\u9fa5]/;
//密码输入框的正则
var password = /[0-9a-zA-Z]{9,16}/;
//电环号码正则
var reg = /^1(3|5|8|7|6)\d{9}$/;
//输入框input监听事件
$(function () {
    //昵称
    $(".input_a input").bind('input propertychange', function () {
        if ($(".input_a input").val() != "") {
            $(".input_b").text("");
        } else {
            $(".input_b").text("昵称不能为空");
        }
    })

    //密码
    $(".input_A input").bind('input propertychange', function () {
        if ($(".input_A input").val() != "") {
            if (password.test($(".input_A input").val())) {
                $(".input_B").text("");
            } else {
                $(".input_B").text("密码格式错误");
            }
        } else {
            $(".input_B").text("密码不能为空");
        }
    })

    //手机号
    $(".cellphoneA input").bind('input propertychange', function () {
        if ($(".cellphoneA input").val() !== "") {
            if (reg.test($(".cellphoneA input").val())) {
                $(".judge").text("");
            } else {
                $(".judge").text("手机号格式错误");
            }
        } else {
            $(".judge").text("手机号不能为空");
        }
    })
})







//第二个输入框里有两个盒子，一个是input。一个是正则显示框
$(".inputB").append($('<div class="input_A"></div><div class="input_B"></div>'));
//密码输入框
$(".input_A").append($('<input type="password" placeholder="密码" id = "Rpassword">'))
//正则

//手机号码那一行
$(".cellphone").append($('<div class="cellphoneA"></div><div class="cellphoneB"></div><ul class="region"></ul>'));//左边手机号区号，右边输入框，选择地区
$(".cellphoneA").append($('<input type="number" placeholder="手机号码" id = "Rdel">'));//手机号码输入框
$(".cellphoneB").append($('<input type="text"><div><img src="../source/image/regist/down.png"></div>'));//手机区号输入框


// 选择地区框
var regionArr = ["中国 +86", "中国台湾地区 +886", "中国香港特别行政区 +852", "中国澳门特别行政区 +853", "阿尔巴尼亚 +355", "阿尔及利亚 +213", "阿富汗 +93", "阿根廷 +54", "爱尔兰 +353", "埃及 +20", "埃塞俄比亚 +251", "爱沙尼亚 +372", "阿拉伯联合酋长国 +971",];//地区数组
for (var i = 0; i < regionArr.length; i++) {
    $(".region").append($('<li>' + regionArr[i] + '</li>'));//循环创建li内容
}
$(".cellphoneB input").val("+ 86");
//点击输入区号的小块
$(".region li").click(function () {
    var s = $(this).text().split("+")[1];
    //var num=s.replace(/[^0-9]/ig,"");//正则
    $(".cellphoneB input").val("+ " + s)
    $(".cellphone ul").slideUp(500);//收起列表
})
$(".cellphoneB div").click(function () {
    $(".cellphone ul").slideDown(500)//下滑列表
})
$(".cellphoneB input").blur(function () {//失去焦点
    $(".cellphone ul").slideUp(500);//收起列表
})
$(".cellphoneB input").focus(function () {//获得焦点
    $(".cellphone ul").slideDown(500)//下滑列表
})


//立即注册的那一行
$(".Immediately").append($('<button class = "logingd">立即注册</button>'));

//隐私政策那一栏
$(".agreement").append($('<div class="privacy"></div><div class="policy"></div>'));//俩部分，显示出来的部分和隐藏的部分
var proto = true;
//显示的那一部分
$(".privacy").append($('<label for="agree" class="checkbox"><img src="../source/image/regist/no.png" alt="">我已阅读并同意相关服务条款和隐私政策</label><img src="../source/image/regist/up.png" alt="">'));//分为三个小块
//同意协议的小图标
$(".checkbox img").click(function () {
    if (proto) {
        proto = false;
        $(this).attr("src", "../source/image/regist/ok.png");
    } else {
        proto = true;
        $(".checkbox img").attr("src", "../source/image/regist/no.png");
    }
})

//隐藏的协议
$(".policy").append($('<a href="">《QQ号码规则》</a><a href="">《隐私政策》</a><a href="">《QQ空间服务协议》</a>'))