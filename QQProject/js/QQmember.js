/* 整个页面分为五大块：导航栏、中间轮播图、轮播图下的四个小块、底部小图标栏，隐藏起来的栏 */
$(".wrap").append($(' <div class="header"></div><div class="theme"></div><div class="content"></div><div class="foot"></div><div class="conceal"></div>'));
//导航栏
$(".header").append($('<div class="headerCenter"></div><div class="detail"><div></div></div>'))
//分三部分，logo部分、选项部分、登陆登出部分（互换）
$(".headerCenter").append($('<a href=""></a><nav class="Option"></nav><div class="login"></div>'));

$(".headerCenter a").append($('<img src="../img/logo4.png" alt="">'));//logo

$(".Option").append($('<ul></ul>'))
//导航栏功能选项
var OptionArr = ["功能特权", "游戏特权", "生活特权", "装扮特权", "会员活动", "成长体系", "年费专区", "大王卡特权"];
// 创建li>a标签
for (var i = 0; i < OptionArr.length; i++) {
    $(".Option ul").append($('<li><a href="">' + OptionArr[i] + '</a></li>'));
}

//萝卜图
var timer;//计时器
var imgindex = 0;//第几张图片的变量
var yuan = [];
$(".theme").append($('<div class="left"></div><div class="matter"></div><div class="right"></div>'));//左右按钮和中间的图
$(".theme .matter").append($('<ul class="matterPhoto"></ul><div class="dot"></div>'));//分为图片部分和圆点部分
var imgArr = ["../img/1.png", "../img/2.png", "../img/3.png", "../img/4.png", "../img/5.png"];//萝卜图图片数组

//图片循环
for (var i = 0; i < imgArr.length; i++) {
    $(".matter .matterPhoto").append($('<li><img src="' + imgArr[i] + '" alt=""></li>'));//图片
    $(".theme .matter .dot").append($('<li></li>'));
}

//下一张的函数
function nextimg() {
    clearInterval(timer);//清除计时器
    imgindex++;
    //最后一张图之后，重置成第一张图
    if (imgindex == imgArr.length) {
        imgindex = 0
    }
    $(".matter .matterPhoto").css("transform", "translateX(" + imgindex * -800 + "px)");
    autoloop()
}
$(".left").click(function () {
    nextimg();
});//点击

//上一张的函数
function lastimg() {
    clearInterval(timer);
    imgindex--;
    if (imgindex == -1) {
        imgindex = imgArr.length - 1;
    }
    $(".matter .matterPhoto").css("transform", "translateX(" + imgindex * -800 + "px)");

}
$(".right").click(function () {
    lastimg()
});
//自动播放
function autoloop() {
    timer = setInterval(function () {
        nextimg();
    }, 5000)
}
autoloop();
//鼠标移入停止计时器
$(".right,.left").mouseenter(function () {
    clearInterval(timer);
});
//鼠标移出的时候让图继续动
$(".right,.left").mouseleave(function () {
    autoloop();
});

$(".content").append($('<ul></ul>'));

var functionArr = [["功能特权", "超级会员 特权王者", "../img/shopA.png"], ["装扮特权", "超级会员 我有我风采", "../img/shopB.png"], ["游戏特权", "游戏礼包 专属福利", "../img/shopC.png"], ["生活特权", "超多优惠福利", "../img/shopD.png"]];//四个小块里的内容

//循环创建四个小块
for (var i = 0; i < functionArr.length; i++) {
    $(".content ul").append($('<li></li>'));
    $(".content ul li").eq(i).append($('<h3>' + functionArr[i][0] + '</h3><p>' + functionArr[i][1] + '</p><img src="' + functionArr[i][2] + '" alt="">'));

}

//底部图标小块
$(".wrap .foot").append($('<div class="footchunk"></div><p>Copyright © 1998 - 2020 Tencent .  All Rights Reserved</p><p>腾讯公司 版权所有</p>'));

$(".wrap .foot .footchunk").append($('<ul></ul>'));
var character = ["官方手Q帐号", "官方微信帐号", "兴趣部落", "官方空间", "花费开通专区", "兑换中心", "客服中心"];//底部小块
for (var i = 0; i < character.length; i++) {
    // <span></span><span></span><div></div>
    $(".wrap .foot .footchunk ul").append($('<li><span style="background-position:0 ' + i * -100 + 'px' + ';"></span><span>' + character[i] + '</span><div></div></li>'));
}

//鼠标移入事件
$(".footchunk span").mouseenter(function () {
    $(this).css("background-position-x", "-100px");
}).mouseleave(function () {
    $(this).css("background-position-x", "");
});


//最底部隐藏栏
$(".conceal").append($('<div class="cont"></div>'))

$(".conceal .cont").append($('<img src="../img/nozuo.png" alt=""><p>开通超级会员,做特权王者</p><a href="">开通超级会员' + "&gt" + '</a><span>开通QQ会员</span>'));//分为4部分

//底部栏的出现与隐藏
var scrollNum; //滚动偏移量
document.onscroll = function () {
    var s1 =window.pageXOffset;
    console.log(s1)
    var s2=document.body.scrollTo;
    console.log(s2)
    var s3=document.documentElement.scrollTo;
    console.log(s3)
    // scrollNum = s1 || s2 || s3;
    // if(scrollNum > 10){
    //    $(".conceal").css("display","block");
    // }else{
    //     $(".conceal").css("display","none");
    // }
}