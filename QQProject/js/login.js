//   注册点击事件
$(".logingd").click(function () {
    //   正则通过之后调用注册账户函数
    //   获取昵称
    var name = $("#Rnick").val();
    //   获取密码
    var pswd = $("#Rpassword").val();
    //   获取电话
    var del = $("#Rdel").val();
    //   昵称非空
    if (name != '') {
        //   密码非空
        if (pswd != '') {
            //   电话非空
            if (del != '') {
                //   正则全局查找空格
                var Nu = / /g;
                //   判断昵称密码电话内没有空格填入
                if (Nu.test(name) || Nu.test(pswd) || Nu.test(del)) {
                    $(".input_b").text("请保证没有空格填入");
                    $(".input_B").text("请保证没有空格填入");
                    $(".judge").text("请保证没有空格填入");
                } else {
                    //   正则判断密码格式
                    var pswdZz = /[a-zA-z0-9!@#$%^&*<>,./?'"|\-+.`~]{6,16}/;
                    if (pswdZz.test(pswd)) {
                        //   正则判断电话格式
                        var delZz = /^1+[0-9]{10}/;
                        if (delZz.test(del)) {
                            //   调用函数进行注册
                            if (!proto) {
                                RetRegirster();
                            } else {
                                $(".input_b").text("请勾选同意QQ协议相关规则");
                                $(".input_B").text("请勾选同意QQ协议相关规则");
                                $(".judge").text("请勾选同意QQ协议相关规则");
                            }
                        } else {
                            $(".judge").text("手机号格式不正确");
                        }
                    } else {
                        $(".input_B").text("密码格式应为6-16位数字字母字符组成");
                    }
                }
            } else {
                $(".judge").text("手机号不能为空");
            }
        } else {
            $(".input_B").text("密码不能为空");
        }
    } else {
        $(".input_b").text("账号不能未空");
    }
})
//   登录点击事件
$(".btn2").click(function () {
    //   获取用户输入
    //   获取账号
    var acc = $("#Laccount").val();
    //   获取密码
    var pswd = $("#Lpassword").val();
    //   判断账号不为空
    if (acc != '') {
        //   判断密码不为空
        if (pswd != '') {
            //   调用登录函数
            RetLogin();
        }
        else {
            console.log("请输入密码");
        }
    } else {
        console.log("请输入账号");
    }
})
//   修改点击事件
$(".upD").click(function () {
    //   获取用户输入
    //   获取账号
    var acc = $("#Laccount").val();
    //   获取密码
    var pswd = $("#Lpassword").val();
    if (acc != '') {
        if (pswd != '') {
            var pswdZz = /[a-zA-z0-9!@#$%^&*<>,./?'"|\-+.`~]{6,16}/;
            if (pswdZz.test(pswd)) {
                upData();
            } else {
                console.log("请正确输入修改的密码格式");
            }
        } else {
            console.log("请正确输入修改的密码");
        }
    } else {
        console.log("请输入账号");
    }
})
//   随机数
function rN(min, max) {
    return Math.round(Math.random() * (max - min) + min);
}
//   注册函数
function RetRegirster() {
    //   昵称   获取昵称输入框值
    var Rname = $("#Rnick").val();
    //   密码   获取密码输入框值
    var Rpswd = $("#Rpassword").val();
    //   电话   获取电话输入框值
    var Rdel = $("#Rdel").val();
    //   账号随机
    var Raccount = String(rN(111111111, 199999999));
    //   ajax请求
    $.ajax({
        //   post类型传输
        type: "post",
        //   请求地址
        url: "http://localhost/QQproject/php/login.php",
        //   同异请求
        async: true,
        //   传输数据
        data: {
            //   类型  注册
            type: "regirster",
            //   昵称
            name: Rname,
            //   密码
            pswd: Rpswd,
            //   电话
            del: Rdel,
            //   账号
            account: Raccount,
        },
        //   请求通过
        success: function (res) {
            //   执行注册成功操作
            // console.log(JSON.parse(res));
            switch (JSON.parse(res).code) {
                case 1:
                    alert(JSON.parse(res).message + "你的账号: " + JSON.parse(res).account);
                    $("#Rnick").val("");
                    $("#Rpassword").val("");
                    $("#Rdel").val("");
                    $(".input_b").text("");
                    $(".input_B").text("");
                    $(".judge").text("");
                    break;
                case 2:
                    alert(JSON.parse(res).message);
                    break;
            }
        }
    })
}
//   登录函数
function RetLogin() {
    //   账号   获取账号输入框值
    var Laccount = $("#Laccount").val();
    //   面膜   获取密码框值
    var Lpswd = $("#Lpassword").val();
    //   ajax请求
    $.ajax({
        //   post类型传输
        type: "post",
        //   请求地址
        url: "http://localhost/QQproject/php/login.php",
        //   同异请求
        async: true,
        //   传输数据
        data: {
            //   类型 登录
            type: "login",
            //   账号
            account: Laccount,
            //   密码
            pswd: Lpswd,
        },
        //   请求通过
        success: function (res) {
            //   登录成功操作
            console.log(JSON.parse(res));
        }
    })
}
function upData() {
    //   获取修改的数据
    //   账号
    var upDt = $("#Laccount").val();
    //   密码
    var upAc = $("#Lpassword").val();
    //   ajax请求
    $.ajax({
        //   请求类型
        type: "post",
        //   请求地址
        url: "http://localhost/QQproject/php/login.php",
        //   同异请求
        async: true,
        data: {
            //   类型  更新
            type: "upData",
            //   密码
            pswd: upAc,
            //   id
            account: upDt,
        },
        success: function (res) {
            console.log(JSON.parse(res));
        },
    })
}