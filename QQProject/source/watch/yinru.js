// 基于准备好的dom，初始化echarts实例
var myChart = echarts.init(document.getElementById('main'));

// 指定图表的配置项和数据
var option = {
    title: {
        text: 'QQ登录某B站查看信息类别',        //主标题文本
        subtext: '不是虚构',            //副标题文本
        x: 'center'                    //标题水平安放位置
    },
//                calculable: true,                        //是否启用拖拽重计算特性，默认关闭
    series: [{                                //数据内容
        name: '访问来源',                        //系列名称，如启用legend，该值将被legend.data索引相关
        type: 'pie',                        //图表类型，必要参数！
        radius: '55%',                        //半径
        center: ['50%', '60%'],                //圆心坐标
        data: [{                            //数据
                value: 335,
                name: '鬼畜'
            },
            {
                value: 310,
                name: '搞笑'
            },
            {
                value: 234,
                name: '漫展'
            },
            {
                value: 135,
                name: '古装'
            },
            {
                value: 1548,
                name: '二次元'
            }
        ]
    }],
    tooltip: {                         //提示框，鼠标悬浮交互时的信息提示
        trigger: 'item',            //触发类型，默认数据触发，可选值有item和axis
        formatter: "{a} <br/>{b} : {c} ({d}%)",    //鼠标指上时显示的数据  a（系列名称），b（类目值），c（数值）, d（占总体的百分比）
        backgroundColor: 'rgba(0,0,0,0.7)'    //提示背景颜色，默认为透明度为0.7的黑色

    },
    legend: {                                //图例，每个图表最多仅有一个图例。
        orient: 'vertical',                    //布局方式，默认为水平布局，可选值有horizontal(竖直)和vertical(水平)
        x: 'left',                            //图例水平安放位置，默认为全图居中。可选值有left、right、center
        data: ['鬼畜', '搞笑', '漫展', '古装', '二次元']
    },
    toolbox: {                                //工具箱，每个图表最多仅有一个工具箱。
        show: true,                            //显示策略，可选为：true（显示） | false（隐藏）
        feature: {                            //启用功能
//                        mark: {                            //辅助线标志
//                            show: true
//                        },
            dataView: {                        //数据视图
                show: true,
                readOnly: false                //readOnly 默认数据视图为只读，可指定readOnly为false打开编辑功能
            },
            restore: {                        //还原，复位原始图表   右上角有还原图标
                show: true
            },
            saveAsImage: {                    //保存图片（IE8-不支持），默认保存类型为png，可以改为jpeg
                show: true,
                type:'jpeg',
                title : '保存为图片'
            }
        }
    }
};

// 使用刚指定的配置项和数据显示图表。
myChart.setOption(option);